#include <Adafruit_NeoPixel.h>
#define PIN    A0 //Pin des LEDs
#define LED_COUNT 90//nb de LEDs
#define PIN1    A1 //Pin des LEDs
#define LED_COUNT1 90 //nb de LEDs
#define PIN2    A2 //Pin des LEDs
#define LED_COUNT2 53 //nb de LEDs
#define PIN3    A3 //Pin des LEDs
#define LED_COUNT3 53 //nb de LEDs
#define PIN4    A4 //Pin des LEDs
#define LED_COUNT4 107//nb de LEDs
#define PIN5    A5 //Pin des LEDs
#define LED_COUNT5 107//nb de LEDs

Adafruit_NeoPixel strip = Adafruit_NeoPixel (LED_COUNT, PIN, NEO_GRB + NEO_KHZ800);
Adafruit_NeoPixel strip1 = Adafruit_NeoPixel (LED_COUNT1, PIN1, NEO_GRB + NEO_KHZ800);
Adafruit_NeoPixel strip2 = Adafruit_NeoPixel (LED_COUNT2, PIN2, NEO_GRB + NEO_KHZ800);
Adafruit_NeoPixel strip3 = Adafruit_NeoPixel (LED_COUNT3, PIN3, NEO_GRB + NEO_KHZ800);
Adafruit_NeoPixel strip4 = Adafruit_NeoPixel (LED_COUNT4, PIN4, NEO_GRB + NEO_KHZ800);
Adafruit_NeoPixel strip5 = Adafruit_NeoPixel (LED_COUNT5, PIN5, NEO_GRB + NEO_KHZ800);

void setup() {

  strip.begin();
  strip.show();

  strip1.begin();
  strip1.show();

  strip2.begin();
  strip2.show();

  strip3.begin();
  strip3.show();

  strip4.begin();
  strip4.show();

  strip5.begin();
  strip5.show();

}

void loop() {
  chargall(0, 0, 250);
}
