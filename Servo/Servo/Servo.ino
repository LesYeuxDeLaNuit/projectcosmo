#include <Servo.h>

const int servo1 = A3;
const int servo2 = A2;
const int joyH = A1;
const int joyV = A0;

int servoVal;

Servo myservo1;
Servo myservo2;



void setup() {
  myservo1.attach(servo1);
  myservo2.attach(servo2);
}


void loop(){

    servoVal = analogRead(joyH);          
    servoVal = map(servoVal, 0, 1023, 0, 180);      
    myservo2.write(servoVal);
    servoVal = analogRead(joyV);           
    servoVal = map(servoVal, 0, 1023, 0, 180);
    myservo1.write(servoVal);
    delay(15);

}

